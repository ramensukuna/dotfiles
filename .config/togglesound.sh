CURRENT_STATE=$(amixer -c $AUDIO_CARD get Master | egrep 'Playback.*?\[o' | egrep -o '\[o.+\]')
if [ $CURRENT_STATE == '[on]' ]; then
	amixer -c $AUDIO_CARD set Master mute 1>&- 2>&-;
    	amixer -c $AUDIO_CARD set Speaker mute 1>&- 2>&-;
    	amixer -c $AUDIO_CARD set Headphone mute 1>&- 2>&-;
else
    	amixer -c $AUDIO_CARD set Master unmute 1>&- 2>&-;
    	amixer -c $AUDIO_CARD set Speaker unmute 1>&- 2>&-;
    	amixer -c $AUDIO_CARD set Headphone unmute 1>&- 2>&-;
fi
