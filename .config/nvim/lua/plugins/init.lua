require("onedark").setup()
require("lualine").setup()
require("nvim-tree").setup()
require("telescope").setup()
